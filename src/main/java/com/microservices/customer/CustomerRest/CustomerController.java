package com.microservices.customer.CustomerRest;

import com.microservices.customer.CustomerRest.repositories.CustomerRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


import com.microservices.customer.CustomerRest.models.Customer;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/customers")
@Api(value="users", description="Everything about customers")
public class CustomerController {

	@Autowired
	  private CustomerRepository rep;
	
	@ApiOperation(value = "View a list of available users",response = List.class)
	  @ApiResponses(value = {
	          @ApiResponse(code = 200, message = "Successfully retrieved list"),
	          @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	          @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	          @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	  }
	  )
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	  public @ResponseBody List<Customer> getAllCustomer() {
	    return rep.findAll();
	  }
    
	@ApiOperation(value = "Search pet with an ID",response = Customer.class)
	@RequestMapping(value = "/{CustId}", method = RequestMethod.GET)
	public Optional<Customer> getById(@PathVariable("CustId") String CustId){
		return rep.findById(CustId);
	}

	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	  public Customer createCustomer(@Valid @RequestBody Customer customer) {
	    rep.save(customer);
	    return customer;
	  }
	
	@RequestMapping(value ="/{CustId}", method = RequestMethod.DELETE)
	public String deletecustomer(@PathVariable String CustId) {
		System.out.println("id"+CustId);
		rep.deleteById(CustId);
		return ("sucess");
	}
	@RequestMapping(value = "/{CustId}", method =RequestMethod.PUT)
	public Customer updateId(@PathVariable("CustId") String id, @RequestBody Customer customer) {
		System.out.println(id);
		customer.setId(id);
		rep.save(customer);
		return customer;
		
	}


	@RequestMapping(value = "/hkb", method = RequestMethod.GET)
	  public String getAllCustomery() {
	    return "hmv";
	  } 

}

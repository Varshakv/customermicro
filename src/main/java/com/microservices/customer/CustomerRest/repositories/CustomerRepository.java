package com.microservices.customer.CustomerRest.repositories;
import com.microservices.customer.CustomerRest.models.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, String> {
	 // Customer findBy_id(String _id);
	}
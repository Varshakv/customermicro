package com.microservices.customer.CustomerRest.models;

import java.util.Date;
import java.util.Objects;

public class CustomerCreditProfile {
	private String creditProfileDate;
	private ValidFor validFor;
	private int creditRiskRating;
	private int creditScore;
	
	
public CustomerCreditProfile(String creditProfileDate, ValidFor validFor, int creditRiskRating, int creditScore) {
		
		this.creditProfileDate = creditProfileDate;
		this.validFor = validFor;
		this.creditRiskRating = creditRiskRating;
		this.creditScore = creditScore;
	}
public String getCreditProfileDate() {
		return creditProfileDate;
	}
public void setCreditProfileDate(String creditProfileDate) {
		this.creditProfileDate = creditProfileDate;
	}
public ValidFor getValidFor() {
		return validFor;
	}
public void setValidFor(ValidFor validFor) {
		this.validFor = validFor;
	}
public int getCreditRiskRating() {
		return creditRiskRating;
	}
public void setCreditRiskRating(int creditRiskRating) {
		this.creditRiskRating = creditRiskRating;
	}
public int getCreditScore() {
		return creditScore;
	}
public void setCreditScore(int creditScore) {
		this.creditScore = creditScore;
	}


}

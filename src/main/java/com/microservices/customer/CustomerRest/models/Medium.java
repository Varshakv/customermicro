package com.microservices.customer.CustomerRest.models;

public class Medium {
    private String city;
    private String country;
	private String emailAddress;
	private String type;
	private String number;
	private String postcode;
	private String stateOrProvince;
    private String street1;
	private String street2;

public Medium(String city, String country, String emailAddress, String type, String number, String postcode,
			String stateOrProvince, String street1, String street2) {
		
		this.city = city;
		this.country = country;
		this.emailAddress = emailAddress;
		this.type = type;
		this.number = number;
		this.postcode = postcode;
		this.stateOrProvince = stateOrProvince;
		this.street1 = street1;
		this.street2 = street2;
	}
public String getCity() {
		return city;
	}
public void setCity(String city) {
		this.city = city;
	}
public String getCountry() {
		return country;
	}
public void setCountry(String country) {
		this.country = country;
	}
public String getEmailAddress() {
		return emailAddress;
	}
public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
public String getType() {
		return type;
	}
public void setType(String type) {
		this.type = type;
	}
public String getNumber() {
		return number;
	}
public void setNumber(String number) {
		this.number = number;
	}
public String getPostcode() {
		return postcode;
	}
public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
public String getStateOrProvince() {
		return stateOrProvince;
	}
public void setStateOrProvince(String stateOrProvince) {
		this.stateOrProvince = stateOrProvince;
	}
public String getStreet1() {
		return street1;
	}
public void setStreet1(String street1) {
		this.street1 = street1;
	}
public String getStreet2() {
		return street2;
	}
public void setStreet2(String street2) {
		this.street2 = street2;
	}
}


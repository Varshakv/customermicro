package com.microservices.customer.CustomerRest.models;
import java.util.Date;
import java.util.Objects;

public class ValidFor {
	private String startDateTime;
	private String endDateTime;
	
public ValidFor(String startDateTime, String endDateTime) {
		
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
	}
public String getStartDateTime() {
		return startDateTime;
	}
public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}
public String getEndDateTime() {
		return endDateTime;
	}
public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}
}

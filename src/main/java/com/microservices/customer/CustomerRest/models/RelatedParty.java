package com.microservices.customer.CustomerRest.models;
import java.util.Date;
import java.util.Objects;

public class RelatedParty {
	private String id;
	private String href;
	private String name;
	private String role;
	
public RelatedParty(String id, String href, String name, String role) {
		
		this.id = id;
		this.href = href;
		this.name = name;
		this.role = role;
	}
public String getId() {
		return id;
	}
public void setId(String id) {
		this.id = id;
	}
public String getHref() {
		return href;
	}
public void setHref(String href) {
		this.href = href;
	}
public String getName() {
		return name;
	}
public void setName(String name) {
		this.name = name;
	}
public String getRole() {
		return role;
	}
public void setRole(String role) {
		this.role = role;
	}

}

package com.microservices.customer.CustomerRest.models;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.microservices.customer.CustomerRest.models.Characteristic;
import com.microservices.customer.CustomerRest.models.ContactMedium;
import com.microservices.customer.CustomerRest.models.CustomerAccountRef;
import com.microservices.customer.CustomerRest.models.CustomerCreditProfile;
import com.microservices.customer.CustomerRest.models.PaymentMeanRef;
import com.microservices.customer.CustomerRest.models.RelatedParty;
import com.microservices.customer.CustomerRest.models.ValidFor;

@JsonSerialize
@JsonDeserialize
@Document(collection = "customers")

public class Customer {
	private String href;
	private String id;
	private String name;
	private String status;
	private String description;
	private ValidFor validFor;
	private String customerRank;
	private List<RelatedParty> relatedPartyRef=new ArrayList<RelatedParty>();
	
	private List<Characteristic> characteristic = new ArrayList<Characteristic>();
	private List<CustomerCreditProfile> customerCreditProfile = new ArrayList<CustomerCreditProfile>();
	private List<CustomerAccountRef> customerAccountRef = new ArrayList<CustomerAccountRef>();
	private List<PaymentMeanRef> paymentMeanRef = new ArrayList<PaymentMeanRef>();
	private List<ContactMedium> contactMedium = new ArrayList<ContactMedium>();
	
	

	public Customer(String href, String id, String name, String status, String description, ValidFor validFor,
			String customerRank, List<RelatedParty> relatedPartyRef, List<Characteristic> characteristic,
			List<CustomerCreditProfile> customerCreditProfile, List<CustomerAccountRef> customerAccountRef,
			List<PaymentMeanRef> paymentMeanRef, List<ContactMedium> contactMedium) {
		super();
		this.href = href;
		this.id = id;
		this.name = name;
		this.status = status;
		this.description = description;
		this.validFor = validFor;
		this.customerRank = customerRank;
		this.relatedPartyRef = relatedPartyRef;
		this.characteristic = characteristic;
		this.customerCreditProfile = customerCreditProfile;
		this.customerAccountRef = customerAccountRef;
		this.paymentMeanRef = paymentMeanRef;
		this.contactMedium = contactMedium;
	}
	public String getHref() {
		return href;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public ValidFor getValidFor() {
		return validFor;
	}
	public void setValidFor(ValidFor validFor) {
		this.validFor = validFor;
	}
	public String getCustomerRank() {
		return customerRank;
	}
	public void setCustomerRank(String customerRank) {
		this.customerRank = customerRank;
	}
	public List<RelatedParty> getRelatedPartyRef() {
		return relatedPartyRef;
	}
	public void setRelatedPartyRef(List<RelatedParty> relatedPartyRef) {
		this.relatedPartyRef = relatedPartyRef;
	}
	public List<Characteristic> getCharacteristic() {
		return characteristic;
	}
	public void setCharacteristic(List<Characteristic> characteristic) {
		this.characteristic = characteristic;
	}
	public List<CustomerCreditProfile> getCustomerCreditProfile() {
		return customerCreditProfile;
	}
	public void setCustomerCreditProfile(List<CustomerCreditProfile> customerCreditProfile) {
		this.customerCreditProfile = customerCreditProfile;
	}
	public List<CustomerAccountRef> getCustomerAccountRef() {
		return customerAccountRef;
	}
	public void setCustomerAccountRef(List<CustomerAccountRef> customerAccountRef) {
		this.customerAccountRef = customerAccountRef;
	}
	public List<PaymentMeanRef> getPaymentMeanRef() {
		return paymentMeanRef;
	}
	public void setPaymentMeanRef(List<PaymentMeanRef> paymentMeanRef) {
		this.paymentMeanRef = paymentMeanRef;
	}
	public List<ContactMedium> getContactMedium() {
		return contactMedium;
	}
	public void setContactMedium(List<ContactMedium> contactMedium) {
		this.contactMedium = contactMedium;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getId() {
		return id;
	}
	@JsonProperty("_id")
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	

	
}

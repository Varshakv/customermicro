package com.microservices.customer.CustomerRest.models;

public class CustomerAccountRef {
	  private String href;
	  private String id;
	  private String name;
	  private String description;
	  private String status;
	  
	  
	public CustomerAccountRef(String href, String id, String name, String description, String status) {
		
		this.href = href;
		this.id = id;
		this.name = name;
		this.description = description;
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

	}


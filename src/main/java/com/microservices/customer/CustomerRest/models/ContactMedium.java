package com.microservices.customer.CustomerRest.models;
import java.util.Date;
import java.util.Objects;

import com.microservices.customer.CustomerRest.models.Medium;
import com.microservices.customer.CustomerRest.models.ValidFor;

public class ContactMedium {
	
	private Boolean preferred;
	private String type;
	private ValidFor validFor;
	private Medium medium;
	
	
	public ContactMedium(Boolean preferred, String type, ValidFor validFor, Medium medium) {
		
		this.preferred = preferred;
		this.type = type;
		this.validFor = validFor;
		this.medium = medium;
	}
	public Boolean getpreferred() {
		return preferred;
	}
	public void setpreferred(Boolean preferred) {
		this.preferred = preferred;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ValidFor getValidFor() {
		return validFor;
	}
	public void setValidFor(ValidFor validFor) {
		this.validFor = validFor;
	}
	public Medium getMedium() {
		return medium;
	}
	public void setMedium(Medium medium) {
		this.medium = medium;
	}
	
	
	
}
